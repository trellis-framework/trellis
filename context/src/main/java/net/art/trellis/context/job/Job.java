package net.art.trellis.context.job;

import net.art.trellis.context.provider.ActionContextProvider;
import net.art.trellis.util.mapper.ModelMapper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Job implements ModelMapper, ActionContextProvider {

    protected ExecutorService getExecutorService(int nThreads) {
        return Executors.newFixedThreadPool(nThreads);
    }

}

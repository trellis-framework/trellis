package net.art.trellis.context.rule;

public abstract class AbstractRule<T> {

    public abstract boolean condition(T t);

}

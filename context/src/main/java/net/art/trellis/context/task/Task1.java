package net.art.trellis.context.task;

import net.art.trellis.context.process.Process1;

public abstract class Task1<TOutput, TInput> extends BaseTask implements Process1<TOutput, TInput> {

    public abstract TOutput execute(TInput t1);

}

package net.art.trellis.context.process;

public interface Process1<TOutput, TInput> extends BaseProcess {

    TOutput execute(TInput param);

}

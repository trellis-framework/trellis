package net.art.trellis.context.action;

import net.art.trellis.context.process.Process;

public abstract class Action<TOutput> extends BaseAction implements Process<TOutput> {

    public abstract TOutput execute();

}

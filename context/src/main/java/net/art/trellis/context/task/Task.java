package net.art.trellis.context.task;

import net.art.trellis.context.process.Process;

public abstract class Task<TOutput> extends BaseTask implements Process<TOutput> {

    public abstract TOutput execute();

}

package net.art.trellis.context.action;

import net.art.trellis.context.process.BaseProcess;
import net.art.trellis.context.provider.ProcessContextProvider;

public abstract class BaseAction implements BaseProcess, ProcessContextProvider {

}

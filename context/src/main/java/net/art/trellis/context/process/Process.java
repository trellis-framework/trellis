package net.art.trellis.context.process;

public interface Process<TOutput> extends BaseProcess {
    TOutput execute ();
}

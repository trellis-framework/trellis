package net.art.trellis.core.constant;

public enum Calendar {
    JALALI,
    MILADI,
    HIJRI
}

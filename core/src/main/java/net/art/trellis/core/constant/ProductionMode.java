package net.art.trellis.core.constant;

public enum ProductionMode {
    PRODUCTION,
    DEVELOPMENT;
}

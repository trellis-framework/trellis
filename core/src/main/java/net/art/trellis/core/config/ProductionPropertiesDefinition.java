package net.art.trellis.core.config;

import net.art.trellis.core.constant.Country;
import net.art.trellis.core.constant.Language;
import net.art.trellis.core.constant.ProductionMode;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Payload;

@ConfigurationProperties("spring.application")
@Validated
@Data
public class ProductionPropertiesDefinition implements Payload {
    private ProductionMode mode = ProductionMode.PRODUCTION;
    private Language language = Language.EN;
    private Country country = Country.US;
}
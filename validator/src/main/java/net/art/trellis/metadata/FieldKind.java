package net.art.trellis.metadata;

public enum FieldKind {
    STRING,
    NUMERIC,
    EMAIL,
    PHONE,
    NATIONAL_CODE,
    FILE,
    IBAN,
    BANK_CARD,

}

package net.art.trellis.ui.web.controller;

import net.art.trellis.context.provider.ActionContextProvider;
import net.art.trellis.util.mapper.ModelMapper;

public interface Api extends ModelMapper, ActionContextProvider {

}

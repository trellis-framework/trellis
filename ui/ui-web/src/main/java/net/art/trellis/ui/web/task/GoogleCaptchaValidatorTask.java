package net.art.trellis.ui.web.task;

import net.art.trellis.http.helper.HttpHelper;
import net.art.trellis.context.task.Task2;
import net.art.trellis.ui.web.helper.WebHelper;
import net.art.trellis.ui.web.infrastructure.GoogleCaptchaService;
import net.art.trellis.ui.web.payload.GoogleCaptchaVerifyResponse;
import org.springframework.stereotype.Service;

@Service
public class GoogleCaptchaValidatorTask extends Task2<GoogleCaptchaVerifyResponse, String, String> implements WebHelper {

    @Override
    public GoogleCaptchaVerifyResponse execute(String secret, String token) {
        return HttpHelper.call(HttpHelper.getHttpInstance("https://www.google.com", GoogleCaptchaService.class).verify(secret, token, getIp()));
    }
}

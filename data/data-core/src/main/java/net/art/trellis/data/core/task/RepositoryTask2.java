package net.art.trellis.data.core.task;

import net.art.trellis.context.process.Process2;
import net.art.trellis.data.core.data.repository.GenericRepository;

public abstract class RepositoryTask2<TRepository extends GenericRepository<?, ?>, TOutput, TInput1, TInput2> extends BaseRepositoryTask<TRepository> implements Process2<TOutput, TInput1, TInput2> {

    public abstract TOutput execute(TInput1 t1, TInput2 t2);

}

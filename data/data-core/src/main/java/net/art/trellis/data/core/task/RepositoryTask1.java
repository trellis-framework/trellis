package net.art.trellis.data.core.task;

import net.art.trellis.context.process.Process1;
import net.art.trellis.data.core.data.repository.GenericRepository;

public abstract class RepositoryTask1<TRepository extends GenericRepository<?, ?>, TOutput, TInput> extends BaseRepositoryTask<TRepository> implements Process1<TOutput, TInput> {

    public abstract TOutput execute(TInput t1);

}

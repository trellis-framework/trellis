package net.art.trellis.data.core.task;

import net.art.trellis.context.process.Process3;
import net.art.trellis.data.core.data.repository.GenericRepository;

public abstract class RepositoryTask3<TRepository extends GenericRepository<?, ?>, TOutput, TInput1, TInput2, TInput3> extends BaseRepositoryTask<TRepository> implements Process3<TOutput, TInput1, TInput2, TInput3> {

    public abstract TOutput execute(TInput1 t1, TInput2 t2, TInput3 t3);

}

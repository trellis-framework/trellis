package net.art.trellis.data.core.task;

import net.art.trellis.context.task.BaseTask;
import net.art.trellis.core.application.ApplicationContextProvider;
import net.art.trellis.data.core.data.repository.GenericRepository;
import net.art.trellis.data.core.util.PagingModelMapper;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;

@Transactional(
        isolation = Isolation.READ_COMMITTED,
        propagation = Propagation.REQUIRES_NEW,
        rollbackFor = {TransactionSystemException.class}
)
public abstract class BaseRepositoryTask<TRepository extends GenericRepository<?, ?>> extends BaseTask implements PagingModelMapper {

    private TRepository repository;

    public void inject(TRepository repository) {
        this.repository = repository;
    }

    public TRepository getRepository() {
        return repository == null ?
                ApplicationContextProvider.context.getBean((Class<TRepository>) ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments()[0]) :
                repository;
    }


}

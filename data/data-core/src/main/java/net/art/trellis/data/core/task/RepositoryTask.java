package net.art.trellis.data.core.task;

import net.art.trellis.context.process.Process;
import net.art.trellis.data.core.data.repository.GenericRepository;

public abstract class RepositoryTask<TRepository extends GenericRepository<?, ?>, TOutput> extends BaseRepositoryTask<TRepository> implements Process<TOutput> {

    public abstract TOutput execute();

}

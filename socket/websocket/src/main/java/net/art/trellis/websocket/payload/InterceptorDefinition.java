package net.art.trellis.websocket.payload;

public class InterceptorDefinition {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

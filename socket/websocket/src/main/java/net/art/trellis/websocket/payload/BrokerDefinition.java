package net.art.trellis.websocket.payload;

public class BrokerDefinition {

    private String broker;

    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }

}

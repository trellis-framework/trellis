package net.art.trellis.websocket.constant;

import net.art.trellis.core.message.MessageHandler;

public enum Messages implements MessageHandler {
    TOKEN_NOT_VALID,
}

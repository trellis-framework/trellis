package net.art.trellis.communication.proto.server.service;

import net.art.trellis.communication.proto.server.provider.ProtoActionContextProvider;
import net.art.trellis.context.provider.ActionContextProvider;
import net.art.trellis.util.mapper.ModelMapper;

public interface Proto extends ModelMapper, ProtoActionContextProvider, ActionContextProvider {

}

package net.art.trellis.message.task;

import net.art.trellis.context.task.Task3;
import net.art.trellis.message.config.SmsPropertiesDefinition;
import net.art.trellis.message.payload.SendMessageResponse;

import java.util.List;

public abstract class AbstractSendBatchSmsTask extends Task3<List<SendMessageResponse>, SmsPropertiesDefinition, List<String>, String> {

}

package net.art.trellis.message.action;

import com.google.common.base.Strings;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import net.art.trellis.context.action.Action2;
import net.art.trellis.core.log.Logger;
import net.art.trellis.message.constant.SendMessageStatus;
import net.art.trellis.message.payload.FireBaseConfiguration;
import net.art.trellis.message.payload.SendFireBaseNotificationRequest;
import net.art.trellis.message.payload.SendMessageResponse;
import net.art.trellis.message.task.BuildFireBaseMessageTask;
import net.art.trellis.message.task.InitializeFireBaseTask;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SendFireBaseNotificationWithConfigurationAction extends Action2<SendMessageResponse, FireBaseConfiguration, SendFireBaseNotificationRequest> {

    @Override
    public SendMessageResponse execute(FireBaseConfiguration configuration, SendFireBaseNotificationRequest request) {
        try {
            FirebaseApp app = call(InitializeFireBaseTask.class, configuration);
            Message message = call(BuildFireBaseMessageTask.class, request);
            String response = FirebaseMessaging.getInstance(app).send(message);
            return SendMessageResponse.of(Optional.ofNullable(response).map(Strings::emptyToNull).map(x -> SendMessageStatus.SUCCESS).orElse(SendMessageStatus.FAILED));
        } catch (Exception e) {
            Logger.error("SendFireBaseNotificationException", e.getMessage());
            return SendMessageResponse.error(e.getMessage());
        }
    }

}

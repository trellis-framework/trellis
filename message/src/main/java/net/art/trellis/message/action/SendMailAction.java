package net.art.trellis.message.action;

import net.art.trellis.context.action.Action1;
import net.art.trellis.message.config.MailPropertiesDefinition;
import net.art.trellis.message.payload.SendMailRequest;
import net.art.trellis.message.payload.SendMessageResponse;
import org.springframework.stereotype.Service;

@Service
public class SendMailAction extends Action1<SendMessageResponse, SendMailRequest> {

    @Override
    public SendMessageResponse execute(SendMailRequest request) {
        return call(SendMailWithConfigurationAction.class, MailPropertiesDefinition.getFromApplicationConfig(), request);
    }

}

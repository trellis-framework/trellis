package net.art.trellis.message.constant;

import net.art.trellis.util.generic.Comparable;

public enum FireBasePlatform implements Comparable<FireBasePlatform> {
    WEB,
    IOS,
    ANDROID,
    ALL;
}

package net.art.trellis.message.action;

import net.art.trellis.context.action.Action1;
import net.art.trellis.core.log.Logger;
import net.art.trellis.message.payload.FireBaseConfiguration;
import net.art.trellis.message.payload.SendFireBaseNotificationRequest;
import net.art.trellis.message.payload.SendMessageResponse;
import org.springframework.stereotype.Service;

@Service
public class SendFireBaseNotificationAction extends Action1<SendMessageResponse, SendFireBaseNotificationRequest> {

    @Override
    public SendMessageResponse execute(SendFireBaseNotificationRequest request) {
        try {
            return call(SendFireBaseNotificationWithConfigurationAction.class, FireBaseConfiguration.getFromApplicationConfig(), request);
        } catch (Exception e) {
            Logger.error("SendFireBaseNotificationException", e.getMessage());
            return SendMessageResponse.ok();
        }
    }

}

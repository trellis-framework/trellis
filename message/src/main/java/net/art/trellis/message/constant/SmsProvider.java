package net.art.trellis.message.constant;

import net.art.trellis.message.task.AbstractSendBatchSmsTask;
import net.art.trellis.message.task.SendBatchSmsWithKavehNegarTask;
import net.art.trellis.message.task.SendBatchSmsWithMagfaTask;

public enum SmsProvider {
    MAGFA(SendBatchSmsWithMagfaTask.class),
    KAVEH_NEGAR(SendBatchSmsWithKavehNegarTask.class);

    private final Class<? extends AbstractSendBatchSmsTask> factory;

    public Class<? extends AbstractSendBatchSmsTask> getFactory() {
        return factory;
    }

    SmsProvider(Class<? extends AbstractSendBatchSmsTask> factory) {
        this.factory = factory;
    }
}

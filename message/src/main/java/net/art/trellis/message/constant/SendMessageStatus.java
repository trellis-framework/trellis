package net.art.trellis.message.constant;

public enum SendMessageStatus {
    SUCCESS,
    FAILED
}
